<?php 
	$name = isset($account->name) ? $account->name : $account->email;
?>
<h1>User {{ $name }} just {{ $action }}</h1>

<p>
<a href="{{ url($showLink) }}">Please confirm</a> this user's registration.
</p>