<?php 
	$name = isset($account->name) ? $account->name : $account->email;
?>
<h1>User {{ $name }} at <a href="mailto:{{ $account->email }}">{{ $account->email }}</a> just {{ $action }}</h1>

<p>
Please help to familiarize this user with the web application.
</p>