<?php 
	$name = isset($account->name) ? $account->name : $account->email;
?>
<h1>You are ready to register for an account</h1>

<p>
Your request for an account has been approved! You may now <a href="{{ url($showLink) }}">begin registration</a>.
</p>