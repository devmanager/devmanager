@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Show Accounts</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@elseif ( Session::get('message') )
						<div class="bg-success">
							<h2>Success!</h2>
							<p>{{ Session::get('message') }}</p>
						</div>
					@endif
						<?php 
							$fullclassname = get_class($accounts);
							$fullclassname = explode("\\", $fullclassname);
							$heading = $fullclassname[count($fullclassname) - 1];
							$heading = str_plural($heading);
							
							$heading_lc = strtolower($heading);
						?>
						<h1 id="tableHeading">{{ $heading }}</h1>
						@include("accounts.$heading_lc")
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
