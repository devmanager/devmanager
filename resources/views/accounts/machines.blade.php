						<?php 
							$connections = Config::get('remote.connections');
						?>
						@foreach ($connections as $conn_name => $conn)
						<?php $table_headings = []; ?>
						<div class="table-responsive">
							<table class="accounts table" id="{{ $heading }}">
								<tbody>
									<tr>
											<?php 
												$table_headings[] = "connection name";
											?>
											<td>
												{{ $conn_name }}
											</td>
											@foreach ($conn as $option => $value)
												<?php
													$class = '';
													/*
													 * Only allow connections with a key
													 */
													if($option === 'password' || is_array($value)){
														continue;
													}
													$table_headings[] = $option;
													
													if($option === 'key'){
												?>
													{{-- Show the key in a div that can be scrolled --}}
													<td class="key">
														<div class="scroll">
														{{ $value }}
														</div>
													</td>
												<?php 		
													} else {
												?>
													<td>
														{{ $value }}
													</td>
												<?php 
													}
												?>
												
											@endforeach
											<td>
												@if (!empty($conn['host']) && !App\Machine::where('host', '=', $conn['host'])->get()->isEmpty())
													Enabled
												@else
													<?php 
													?>
													Not Enabled | <a href="/machines/create?conn={{ $conn_name }}">Create</a>
												@endif
											</td>
									</tr>
								</tbody>
								<thead>
									<tr>
										<?php 
											$table_headings[] = 'enabled';
										?>
										@foreach ($table_headings as $th)
											<th>{{ $th }}</th>											 
										@endforeach
									</tr>
								</thead>
							</table>
						</div>
						@endforeach