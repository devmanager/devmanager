@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
		@if ( Session::get('message') )
		<div class="bg-success">
			<h2>Success!</h2>
			<p>{{ Session::get('message') }}</p>
		</div>
		@endif
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{!! $error !!}</li>
					@endforeach
				</ul>
			</div>
		@endif
			<?php 
				$dashboard = new App\Dashboard\ProjectDashboard\ProjectDashboard();
				echo $dashboard->getView($data);						
			?>
		</div>
	</div>
</div>
@endsection
