						@if ($accounts->all()->isEmpty())
							<h2>There are no records</h2>
						@else
							<?php
							 	$table_headings = $accounts->getFillable();
							?>
							<div class="table-responsive">
								<table class="accounts table" id="{{ $heading }}">
									<tbody>
										<tr>
											{{-- Views should be named after the model's lowercase name --}}									
											<?php $records = $accounts->all(); ?>
											@foreach ($records as $record)
												@foreach ($table_headings as $prop)
													<td>												
														@if ( $prop === 'confirmed' )
															
																<form role="form" action="/confirmations/{{ $record->token }}" method="post" class="confirmation_status_form">
																	<input type="hidden" name="_token" value="{{ csrf_token() }}">																
																	<input type="hidden" value="{{ $record->email }}" name="email" />
																	<input type="hidden" name="_method" value="put">
																	@if ( !$record->$prop )
																		<div class="confirmation_status">No</div>
																		<button class="form-control btn btn-default" type="submit" name="submit" value="Approve">Approve</button>
																	@else
																		<div class="confirmation_status">Yes</div>
																	@endif
																	<button class="form-control btn btn-default" type="submit" name="submit" value="Remove">Remove</button>
																</form>															
														@else
															{{ $record->$prop }}
														@endif
													</td>								 
												@endforeach											 
											@endforeach
										</tr>
									</tbody>
									<thead>
										<tr>
											@foreach ($table_headings as $th)
												<th>{{ $th }}</th>											 
											@endforeach
										</tr>
									</thead>
								</table>
							</div>
						@endif