	<?php 
		$connections = Config::get('remote.connections');
	?>
	@foreach ($connections as $conn_name => $conn)
		<?php $table_headings = []; ?>
		@if (!empty($conn['projects']))
			{{-- Show a table for each remote connection that has projects --}}
			<div class="table-responsive">
				<h2>{{ $conn_name }}</h2>
				<table class="accounts table" id="{{ $heading }}">
					<tbody>
						@foreach ($conn['projects'] as $project)
							<tr>
								@foreach ($project as $field => $value)
							<?php 
								$table_headings[] = $field;   
							?>
								<td>
									@if ($value === false)
										No
									@elseif ($value === true)
										Yes
									@elseif ($field === 'defaultGroup')
										<span title="{{ $value['name'] }}">{{ $value['label'] }}</span>
									@else
										{{ $value }}
									@endif
								</td>
								@endforeach
									<?php
										$field = 'enabled'; 
										$table_headings[] = $field;   
									?>
									<td>
										@if (App\Project::where('name', '=', $project['name'])->get()->isEmpty())
											No | <a href="/projects/create?conn={{ $conn_name }}&proj={{ $project['name'] }}">Enable</a> 
										@else
											Yes
										@endif										
									</td>
							</tr>
						@endforeach
					</tbody>
					<thead>
						<tr>
							@foreach ($table_headings as $th)
								<th>{{ $th }}</th>									 
							@endforeach
						</tr>
					</thead>
				</table>
			</div>
		@endif		
	@endforeach
	