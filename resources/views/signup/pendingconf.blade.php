@extends('app')

@section('content')
<div class="container-fluid">
	<p>Your confirmation is now pending. You will receive email at <b>{{ $conf->email }}</b> from an admin when your request has been confirmed.</p>
</div>
@endsection
