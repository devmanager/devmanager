@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">New Machine</div>					
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@elseif ( Session::get('message') )
						<div class="bg-success">
							<h2>Success!</h2>
							<p>{{ Session::get('message') }}</p>
						</div>
					@endif
					<form class="form-horizontal" role="form" method="POST" action="/machines" id="newMachine">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="conn" value="{{ $connectionData['Connection Name'] }}">
						{{-- Show the key in a smaller container --}}
						@foreach ($connectionData as $label => $data)
							<?php
								// The container for non-editable values
								$container = 'p ';
								$attrs = '';
								if($label === 'password' || $label === 'projects') continue; // Only the key will be used to connect
								$class = '';
								if($label === 'key'){
									$class = ' data';
									$container = 'textarea ';
									$attrs = 'readonly=true ';
								}
							?>
							<div class="form-group">
								<label class="control-label col-sm-2">{{ $label }}</label>
								<div class="col-sm-10 {{ $class }}">
									<{{ $container }}{{ $attrs }}class="form-control-static">{{ $data }}</{{ $container }}>
								</div>
							</div>
						@endforeach
						<div class="form-group">
							<label class="control-label col-sm-2" for="group">Group</label>
							<input type="text" class="form-control" name="group" id="group" value="developers">
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="submit">Enable this machine?</label>
							<button type="submit" name="submit" id="submit" value="enable" class="btn btn-default">Yes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection