@extends('app')

@section('content')
<?php
	$project = $data['project'];
	$projectData = $data['projectData'];
	$groupList = $data['groupList'];
	$shellList = $data['shellList'];
	$connectionName = $project->machine->conn;
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Register for {{ $project->name }}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{!!$error !!}</li>
								@endforeach
							</ul>
						</div>
					@elseif ( Session::get('message') )
						<div class="bg-success">
							<h2>Success!</h2>
							<p>{{ Session::get('message') }}</p>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" autocomplete="off" action="{{ '/accounts/' . $project->id }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Machine Name</label>
							<div class="col-md-6">
								<p class="form-control-static">{{ $connectionName }}</p>								
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Username</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="username" value="{{ old('username') }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password" value="{{ old('password') }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Group List</label>
							<div class="col-md-6">
								<?php 
									$groupArray = json_decode($groupList, true);						
								?>
								{!! Form::select('groupList', $groupArray, $projectData['defaultGroup']['name'], ['class' => 'form-control', 'name' => 'group']) !!}
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Shell List</label>
							<div class="col-md-6">
								<?php 
									$shellArray = json_decode($shellList);
									foreach($shellArray as $key => $val){
										$shellArray[$val] = $val;
										unset($shellArray[$key]);
									}					
								?>
								{!! Form::select('shellList', $shellArray, $projectData['defaultShell'], ['class' => 'form-control', 'name' => 'shell']) !!}
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Sign Up
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
