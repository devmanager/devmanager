@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">New Project - {{ \Input::get('proj') }}</div>					
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@elseif ( Session::get('message') )
						<div class="bg-success">
							<h2>Success!</h2>
							<p>{{ Session::get('message') }}</p>
						</div>
					@endif
					<form class="form-horizontal" role="form" method="POST" action="/projects" id="newProject">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="conn" value="{{ \Input::get('conn') }}">
						<input type="hidden" name="proj" value="{{ \Input::get('proj') }}">
						{{-- Show the key in a smaller container --}}
						@foreach ($projectData as $label => $data)
							<?php
								// The container for non-editable values
								$container = 'p ';
								$attrs = '';
								$value = '';
								$escape = true;
							?>
							<div class="form-group">
								<label class="control-label col-sm-2">{{ $label }}</label>
								<div class="col-sm-10">
									<?php 
										if($data === false){
											$value = 'No';
										} elseif($data === true){
											$value = 'Yes';
										} elseif($label === 'defaultGroup') {
											$value = "<span title='{$data['name']}'>{$data['label']}</span>";
											$escape = false;
										} else {
											$value = $data;
										}
									?>
									<{{ $container }}{{ $attrs }}class="form-control-static">
										@if($escape)
											{{ $value }}
										@else
											{!! $value !!}
										@endif
									</{{ $container }}>
								</div>
							</div>
						@endforeach
						<div class="form-group">
							<label class="col-sm-2 control-label" for="submit">Enable this machine?</label>
							<button type="submit" name="submit" id="submit" value="enable" class="btn btn-default">Yes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection