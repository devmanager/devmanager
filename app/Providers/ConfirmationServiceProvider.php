<?php namespace App\Providers;

class ConfirmationServiceProvider extends AppServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'App\Contracts\Auth\ConfirmationRegistrar',
			'App\Services\ConfirmationRegistrar'
		);
	}

}
