<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MachineAccountServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
				'App\Contracts\MachineAccountRegistrar',
				'App\Services\MachineAccountRegistrar'
		);
	}

}
