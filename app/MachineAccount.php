<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineAccount extends Model {
	protected $guarded = ['id', 'created_at', 'updated_at'];
	
	public function machine(){
		return $this->belongsTo('App\Machine');
	}
	
	public function user(){
		return $this->belongsTo('App\User');
	}
	
	public function project(){
		return $this->belongsTo('App\Project');
	}

}
