<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
/**
 * A machine that a user can can connect to in order to work on pre-defined projects
 * @author Spencer
 *
 */
class Machine extends Model {
	/*
	 * Guarded fields can be changed by admins
	 */
	protected $guarded = ['id', 'created_at', 'updated_at'];
	
	/**
	 * Run the specified command on this machine
	 * 
	 * @param string $commandString
	 * @param string $prefix
	 * @param \Closure $onSuccess
	 * @return Response|array
	 */
	public function command($commandString = '', $prefix = 'An error occured', \Closure $onSuccess = null){
		$connectionName = $this->conn;
		return self::runCommand($connectionName, $commandString, $prefix, $onSuccess);
	}
	
	/**
	 * Run the specified command on any machine found containing projects in remote.php
	 * 
	 * @return Response|array
	 */
	public static function runCommand($connectionName = '', $commandString = '', $prefix = 'An error occured', \Closure $onSuccess = null){
		if(! \Config::get("remote.connections.$connectionName.projects")){
			$errors = ['message' => "Machine <b>$connectionName</b> not found"];
			return redirect()->back()->withErrors($errors);
		}
		
		$commands = [ $commandString ];
		
		$response = '';
		
		\SSH::into($connectionName)->run($commands, function($resp) use (&$response){
			$response = trim($resp);
		});
	
		$message = '';
		$status = '';
		
		// Add the machine
		if(!empty($response)){
			/*
			 * We are expecting the response to be a JSON object with certain keys
			 */
			if($responseData = json_decode($response, true)){
				$status = $responseData['status'];
				$message = $responseData['message'];
				
				if(!empty($responseData['data'])) $data = $responseData['data'];
			} else {
				$status = 'Error';
				$message = "Could not create response data: $response";
			}
	
			if($status === 'Success'){
				if($onSuccess){
					$redirect = $onSuccess($status, $message);
					if($redirect instanceof RedirectResponse){
						return $redirect;
					}
				}		
			}
		} else {
			$status = 'Error';
			$message = 'No response data from shell command';
		}
		
		if($status === 'Error'){
			if(!empty($message)){
				$displayMessage = "$prefix: $message";
			} else {
				$displayMessage = $prefix;
			}
				
			$errors = ['message' => $displayMessage];
			return redirect()->back()->withErrors($errors);
		} else {
			if(empty($data)){
				return redirect('/machines')->with('message', $message);
			} else {
				return [ 'status' => $status, 'message' => $message, 'data' => $data ];
			}
			
		}
	}
}
