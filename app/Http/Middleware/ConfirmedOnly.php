<?php namespace App\Http\Middleware;

use Closure;
use App\Confirmation;

class ConfirmedOnly {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$confirmed = Confirmation::where('token', '=', $request->get('token'))->where('email', '=', $request->get('email'));
		if($confirmed->get()->isEmpty()){
			abort(404);
		}
		return $next($request);
	}

}
