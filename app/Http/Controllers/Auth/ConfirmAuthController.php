<?php namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use App\Contracts\Auth\ConfirmationRegistrar;
use App\Confirmation;
use App\Events\UserSignedUp;

use Illuminate\Http\Request;
use Session;
use Event;

/**
 * Controls the registration of users into the confirmations table
 * @author spencerw
 *
 */
class ConfirmAuthController extends AuthController {

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\ConfirmationRegistrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, ConfirmationRegistrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
	
		$this->middleware('guest', ['except' => ['getLogout', 'getIndex', 'getSignedUp']]);
		$this->middleware('auth', ['except' => ['getLogin', 'getRegister', 'postRegister', 'postLogin', 'getSignedUp']]);
	}
	
	/**
	 * The user's dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getIndex(){
		return view('home');
	}
	
	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function getRegister(){
		return view('signup.register');
	}
	
	/**
	 * Handle a registration request for the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request){
		$validator = $this->registrar->validator($request->all());
	
		if ($validator->fails())
		{
			$this->throwValidationException(
				$request, $validator
			);
		}
	
		if($conf = $this->registrar->create($request->all())){
			/*
			 * User was succesfully added to confirmations. Notify the admins
			 */
			Event::fire(new UserSignedUp($conf, $this->auth->user()));
		} else {
			abort(500, 'An error occured while signing up');
		}		
		return redirect('/signed-up')->with('conf', $conf);
	}
	
	/**
	 * Shown to users right after registering for confirmation
	 * @return \Illuminate\Http\Response
	 */
	public function getSignedUp(){
		$conf = Session::get('conf');
		if(!empty($conf) && !Confirmation::find($conf->token)){
			return view('signup.pendingconf')->with('conf', $conf);
		} else {
			abort(404);
		}
	}
	
	/**
	 * Get the post register / login redirect path.
	 *
	 * @return string
	 */
	public function redirectPath()
	{
		if (property_exists($this, 'redirectPath'))
		{
			return $this->redirectPath;
		}
	
		return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
	}
	
	/**
	 * Get the path to the login route.
	 *
	 * @return string
	 */
	public function loginPath()
	{
		return property_exists($this, 'loginPath') ? $this->loginPath : '/login';
	}
}
