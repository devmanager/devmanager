<?php namespace App\Http\Controllers;

use App\Confirmation;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contracts\Auth\InvitationRegistrar;
use App\Events\UserRegistered;
use App\Role;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

use Event;

/**
 * Control registration into the User model
 * @author spencerw
 *
 */
class ConfirmRegController extends Controller {
	public function __construct(Guard $auth, InvitationRegistrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
	
		$this->middleware('confirm');
	}
	
	/**
	 * Show the registration form
	 * @return \Illuminate\View\View
	 */
	public function getRegister(){
		return view('signup.conregister');
	}
	
	/**
	 * Submit the user's registration data
	 * @param Request $request
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function postRegister(Request $request){
		$validator = $this->registrar->validator($request->all());
	
		if ($validator->fails())
		{
			$this->throwValidationException(
					$request, $validator
			);
		}
	
		if($user = $this->registrar->create($request->all())){
			/*
			 * User was succesfully added to User model. Authenticate the user, remove them
			 * from Confirmations, and notify the admins
			 */
			
			$userRole = Role::where('name', '=', 'user')->first();
			$user->attachRole($userRole);
			
			$this->auth->login($user);
			
			if($conf = Confirmation::find($request->get('token'))){
				$conf->delete();
			}
			
			Event::fire(new UserRegistered($user));
		} else {
			abort(500, 'An error occured while signing up');
		}
		$message = 'Congratulations! You made it!';
		return redirect('/')->with('message', $message);
	}
}
