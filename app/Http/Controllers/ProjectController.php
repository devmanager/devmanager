<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Project;
use App\Machine;
use App\Contracts\ProjectRegistrar;

use Illuminate\Http\Request;

class ProjectController extends Controller {
	public function __construct(Project $project, ProjectRegistrar $registrar, Request $request){
		$this->middleware('admin');
		$this->project = $project;
		$this->registrar = $registrar;
		$this->request = $request;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('accounts.index')->with('accounts', $this->project);
	}

	/**
	 * Show the form for creating a new project.
	 *
	 * @return Response
	 */
	public function create()
	{
		$request = $this->request;
		
		/*
		 * Accept the request if a "conn" query field was given with the name of a connection
		 * in remote.php.
		 */
		$connectionName = $request->input('conn');
		$projectName = $request->input('proj');
		$projects = \Config::get("remote.connections.$connectionName.projects");
		
		foreach($projects as $project){
			if($project['name'] === $projectName){
				$projectData = $project;
				break;
			}
		}
		
		/*
		 * Only proceed if a connection was given that is found in remote.php
		 */
		if(empty($projectData)){
			$message = "Project not found: Connection: $connectionName, Project: $projectName";
			$errors = ['message' => $message];
			return redirect('/projects')->withErrors($errors);
		} else {
			$data = ['projectData' => $projectData];
			return view('signup.newproject')->with($data);
		}
	}

	/**
	 * Store a newly created project in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$request = $this->request;
		$registrar = $this->registrar;
		
		/*
		 * Accept the request if a "conn" query field was given with the name of a connection
		 * in remote.php.
		 */
		$connectionName = $request->input('conn');
		$projectName = $request->input('proj');
		$projects = \Config::get("remote.connections.$connectionName.projects");
		
		foreach($projects as $project){
			if($project['name'] === $projectName){
				$projectData = $project;
				break;
			}
		}
		
		/*
		 * Only proceed if a connection was given that is found in remote.php
		 */
		if(empty($projectData)){
			$message = "Project not found: Connection: $connectionName, Project: $projectName";
			$errors = ['message' => $message];
			return redirect('/projects')->withErrors($errors);
		}
		
		$machineResults = Machine::where('conn', '=', $connectionName)->get();
		
		if($machineResults->isEmpty()){
			$message = "Could not find Machine with connection name: $connectionName";
			$errors = ['message' => $message];
			return redirect('/projects')->withErrors($errors);
		}
		
		$machine = $machineResults->first();
		
		$machine_id = $machine->id;
		
		$data = [
				'name' => $projectName,
				'machine_id' => $machine_id
		];

		$validator = $registrar->validator($data);
		
		// Validate the input
		if ($validator->fails()){
			$this->throwValidationException(
					$request, $validator
			);
		}
		
		$onSuccess = function(&$status, &$message) use (&$registrar, $data){
			if($registrar->create($data)){
				// The machine was added succesfully
				return redirect('/projects')->with('message', $message);
			} else {
				$status = 'Error';
				$message = "Could not register new project";
			}
		};
		
		$commandString = $projectData['setup'] . ' ' . escapeshellarg($projectData['defaultShell']);
		$prefix = "An error occured while creating a new project";
		// Create the project
		return $machine->command($commandString, $prefix, $onSuccess);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
}
