<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Confirmation;
use App\Events\UserConfirmed;

use Illuminate\Http\Request;

use Event;
use Auth;

/**
 * Admin only controller for managing confirmations
 * @author spencerw
 *
 */
class AdminConfirmationController extends Controller {
	public function __construct(Confirmation $conf, Request $request){
		$this->conf = $conf;
		$this->request = $request;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('accounts.index')->with(['accounts' => $this->conf]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	}

	/**
	 * Update the specified resource in storage. Most often, a confirmations record
	 * is being confirmed in order to start making the User.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$conf = $this->conf->find($id);
		$request = $this->request;
		
		$match = $request->input('email') === $conf->email; 
		if(!$conf || !$match){
			abort(404);
		} else {			
			if($request->input('submit') === 'Approve'){
				$conf->confirmed = true;
				$conf->save();
				/*
				 * Send admins email notification that a user was confirmed
				 */
				Event::fire(new UserConfirmed($conf, Auth::user()));				
				$message = "User with email $conf->email confirmed";				
				return redirect()->back()->with('message', $message); 
			} else if ($request->input('submit') === 'Remove') {
				 $conf->delete();
				 
				 $message = "Deleted user with email $conf->email";
				 return redirect()->back()->with('message', $message);
			} else {
				$message = 'No command found';
				$errors = ['message' => $message];	
				return redirect()->back()->withErrors($errors); 
			}
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
