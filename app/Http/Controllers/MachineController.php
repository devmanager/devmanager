<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contracts\MachineRegistrar;

use App\Machine;

use Illuminate\Http\Request;

class MachineController extends Controller {

	public function __construct(Machine $machine, Request $request, MachineRegistrar $registrar){
		$this->middleware('admin'); // Some restrictions may loosened down the road
		$this->machine = $machine;
		$this->request = $request;
		$this->registrar = $registrar;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('accounts.index')->with('accounts', $this->machine);
	}

	/**
	 * Show the form for creating a new machine.
	 *
	 * @return Response
	 */
	public function create()
	{
		/*
		 * Accept the request if a "conn" query field was given with the name of a connection
		 * in remote.php.
		 */
		$request = $this->request;
		$connectionName = $request->input('conn');
		$connectionData = \Config::get("remote.connections.$connectionName") ? \Config::get("remote.connections.$connectionName") : '';
		/*
		 * Only proceed if a connection was given that is found in remote.php
		 */
		if(empty($connectionData)){
			$message = 'No connection found';
			$errors = ['message' => $message];
			return redirect('/machines')->withErrors($errors);
		} else {
			$connectionData = ['Connection Name' => $connectionName] + $connectionData;			
			$data = ['connectionData' => $connectionData];
			return view('signup.newmachine')->with($data);
		}
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$request = $this->request;
		$registrar = $this->registrar;
		
		$connectionName = $request->input('conn');
		$connectionData = \Config::get("remote.connections.$connectionName") ? \Config::get("remote.connections.$connectionName") : '';
		/*
		 * Only proceed if a connection was given that is found in remote.php
		 */
		if(empty($connectionData)){
			$message = 'No connection found';
			$errors = ['message' => $message];
			return redirect('/machines')->withErrors($errors);
		}
		
		$data = [
			'host' => $connectionData['host'],
			'username' => $connectionData['username'],
			'group' => $request->input('group'),
			'key' => $connectionData['key'],
			'conn' => $connectionName,
			'enabled' => true
		];
		$validator = $registrar->validator($data);

		// Validate the input
		if ($validator->fails()){
			$this->throwValidationException(
					$request, $validator
			);
		}
		
		$onSuccess = function(&$status, &$message) use (&$registrar, $data){
			if($registrar->create($data)){
				// The machine was added successfully
				return redirect('/machines')->with('message', $message);
			} else {
				$status = 'Error';
				$message = "Could not register new machine";
			}
		};
		
		$commandString = $connectionData['setup'] . ' ' . escapeshellarg($request->input('group'));
		$prefix = "An error occured while creating a new machine";
		// Test the connection
		return Machine::runCommand($connectionName, $commandString, $prefix, $onSuccess);
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
