<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Contracts\MachineAccountRegistrar;
use App\Project;
use App\Machine;
use App\MachineAccount;

use Auth;
use Input;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class MachineAccountController extends Controller {

	public function __construct(Request $request, MachineAccountRegistrar $registrar){
		$this->middleware('auth');
		$this->request = $request;
		$this->registrar = $registrar;		
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new account on a machine which 
	 * will be used to manage a project.
	 *
	 * @return Response
	 */
	public function create(Project $project)
	{
		$projectData = $project->getConfigData();
		$machine = $project->machine;
		if(!$projectData){
			$errors = ['message' => "Project <b>$project->name</b> not found"];
			return redirect()->back()->withErrors($errors);
		}		
		
		$commandString = $projectData['getGroups'];
		$prefix = 'An error occured retriving groups';
		
		$response = $machine->command($commandString, $prefix);
		if($response instanceof RedirectResponse){
			return $response;
		}
		$groupList = $response['data'];
		
		$commandString = $projectData['getShells'];
		$response = $machine->command($commandString, $prefix);
		if($response instanceof RedirectResponse){
			return $response;
		}
		$shellList = $response['data'];
		
		$data = [
				'project' => $project,
				'projectData' => $projectData,
				'groupList' => $groupList,
				'shellList' => $shellList
		];
		return view('signup.newmachineaccount')->with('data', $data);
	}

	/**
	 * Store a newly created machine account in storage.
	 *
	 * @return Response
	 */
	public function store(Project $project)
	{
		$request = $this->request;
		$registrar = $this->registrar;
		
		$username = Input::get('username');
		$password = Input::get('password');
		$group = Input::get('group');
		$shell = Input::get('shell');
		
		$projectData = $project->getConfigData();
		$connectionName = $project->machine->conn;
		$machine = $project->machine;
		$managingGroup = $projectData['managingGroup'];		
		
		$data = [
			'user_id' => Auth::user()->id,
			'project_id' => $project->id,
			'machine_id' => $project->machine->id,
			'username' => $username,
			'password' => $password,
			'password_confirmation' => Input::get('password_confirmation'),
			'group' => $group,
			'shell' => $shell
		];
		
		$validator = $registrar->validator($data);
		
		// Validate the input
		if ($validator->fails()){
			$this->throwValidationException(
					$request, $validator
			);
		}
		
		$onSuccess = function(&$status, &$message) use (&$registrar, $data, $project){
			if($registrar->create($data)){
				// The account was added succesfully
				// Add the project to the pivot table to associate it with the user
				Auth::user()->addProject($project);
				return redirect('/')->with('message', $message);
			} else {
				$status = 'Error';
				$message = "Could not create new machine account";
			}
		};
		
		$args = [
				escapeshellarg($username), 
				escapeshellarg($password), 
				escapeshellarg($group),
				escapeshellarg($managingGroup),
				escapeshellarg($shell)];		
		$commandString = $projectData['addUser'] . ' ' . implode(' ', $args);
		$prefix = "An error occured while creating a new project";
		// Create the project
		return $machine->command($commandString, $prefix, $onSuccess);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		/*
		 * Only proceed if the MachineAccount belongs to the user
		 */		
		$machineAccount = MachineAccount::findOrFail($id);
		if(!($machineAccount->user->id === Auth::user()->id)){
			$displayMessage = 'Machine Account not registered to user';
			$errors = ['message' => $displayMessage];
			return redirect()->back()->withErrors($errors);
		}
		$request = $this->request;
		
		$project = $machineAccount->project;
		$machine = $machineAccount->machine;
		
		$projectData = $project->getConfigData();
		
		$commandString = $projectData['getGroups'];
		$prefix = 'An error occured retriving groups';
		
		$response = $machine->command($commandString, $prefix);
		if($response instanceof RedirectResponse){
			return $response;
		}
		$groupList = $response['data'];
		
		if(!($groupList = json_decode($groupList, true))){
			$displayMessage = "Could not decode group list: $groupList";
			$errors = ['message' => $displayMessage];
			return redirect()->back()->withErrors($errors);
		}
		
		$data = [
			'groupList' => $groupList 	
		];
		
		return view('accounts.projectdashboard')->with('data', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
