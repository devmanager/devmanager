<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'admin'], function(){
	Route::resource('confirmations', 'AdminConfirmationController');
});

Route::resource('machines', 'MachineController');
Route::resource('projects', 'ProjectController');
Route::resource('accounts', 'MachineAccountController');
Route::get('accounts/create/{project}', 'MachineAccountController@create');
Route::post('accounts/{project}', 'MachineAccountController@store');

Route::controllers([	
	'confirm' => 'ConfirmRegController',
	'password' => 'Auth\PasswordController',
	'/' => 'Auth\ConfirmAuthController'
]);