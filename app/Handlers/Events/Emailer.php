<?php namespace App\Handlers\Events;

use App\Contracts\Events\UserEvent;
use App\User;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

use Mail;

/**
 * Send out email notifactions in response to events
 * @author Spencer
 *
 */
class Emailer {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event. Notify all admins that a user signed up.
	 *
	 * @param  UserSignedUp  $event
	 * @return void
	 */
	public function handle(UserEvent $event)
	{	
		$lists = $event->getMailLists();
		$lists->each(function($bag) use ($event) {
			$data = $event->getViewData();
			$view = $bag->get('view');
			$bag->get('list')->each(function($user) use ($view, $data) {				
				Mail::send("emails.$view", $data, function($message) use ($user) {
					$message->from('spencer@aninternetpresence.net', 'Dev Manager');
					$message->to($user->email);
					$message->subject('[Dev Manager] Notification');
				});
			});
		});		
	}
}
