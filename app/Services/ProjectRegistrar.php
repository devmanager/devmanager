<?php namespace App\Services;

use Validator;
use App\Contracts\ProjectRegistrar as RegistrarContract;
use App\Project;

class ProjectRegistrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'name' => 'required|max:255',
			'machine_id' => 'required|exists:machines,id'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return Project::create([
			'name' => $data['name'],
			'machine_id' => $data['machine_id'],
		]);
	}

}
