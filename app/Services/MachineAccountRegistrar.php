<?php namespace App\Services;

use App\MachineAccount;

use Validator;
use App\Contracts\MachineAccountRegistrar as RegistrarContract;

class MachineAccountRegistrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		$linuxUserTest = 'required|regex:/^[a-z_][a-z0-9_]{0,30}$/';
		$linuxGroupTest = 'required|regex:/^[a-z_][a-z0-9_-]{0,30}$/';
		
		return Validator::make($data, [
			'user_id' => 'required|exists:users,id',
			'project_id' => 'required|exists:projects,id',
			'machine_id' => 'required|exists:machines,id',
			'username' => $linuxUserTest,
			'password' => 'required|max:100|confirmed',
			'group' => $linuxGroupTest,
			'shell' => 'required|string'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return MachineAccount
	 */
	public function create(array $data)
	{
		return MachineAccount::create([
			'user_id' => $data['user_id'],
			'project_id' => $data['project_id'],
			'machine_id' => $data['machine_id'],
			'username' => $data['username'],
			'group' => $data['group'],
			'shell' => $data['shell']
		]);
	}

}
