<?php namespace App\Services;

use Validator;
use App\Contracts\MachineRegistrar as RegistrarContract;
use App\Machine;

class MachineRegistrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'group' => 'regex:/^[a-z_][a-z0-9_]{0,30}$/'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return Machine
	 */
	public function create(array $data)
	{
		return Machine::create([
			'host' => $data['host'],
			'username' => $data['username'],
			'group' => $data['group'],
			'conn' => $data['conn'],
			'key' => $data['key'],
			'enabled' => $data['enabled']
		]);
	}

}
