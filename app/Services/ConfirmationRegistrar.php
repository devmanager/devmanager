<?php namespace App\Services;

use App\Confirmation;
use Validator;
use App\Contracts\Auth\ConfirmationRegistrar as RegistrarContract;

/**
 * Manage the registration of users for pending confirmation
 * @author spencerw
 *
 */
class ConfirmationRegistrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'email' => 'required|email|max:255|unique:confirmations|unique:users'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	public function create(array $data)
	{
		return Confirmation::create([
			'email' => $data['email'],
			'token' => sha1(crypt(mt_rand(), mt_rand())),
		]);
	}

}
