<?php namespace App\Events;

use App\Events\Event;
use App\Contracts\Events\UserEvent;
use App\User;

use Illuminate\Queue\SerializesModels;

use Config;

/**
 * An event for when a user is registered into the User model
 * @author spencerw
 *
 */
class UserRegistered extends Event implements UserEvent {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @param \App\User $user The user who confirmed the new user
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * The data for the view
	 *
	 * @return array
	 */
	public function getViewData(){
		
		return ['action' => 'registered',
				'showLink' => "confirmations",
				'account' => $this->user
		];
	}
	
	public function getMailLists(){
		$mailinglist = [];
		/*
		 * Get list of admins
		*/
		$admins = User::getAdmins();
	
		$mailinglist[] = collect(['list' => $admins, 'view' => 'registered']);
		return collect($mailinglist);
	}
}
