<?php namespace App\Events;

use App\Events\Event;
use App\Contracts\Events\UserEvent;
use App\Confirmation;
use App\User;

use Illuminate\Queue\SerializesModels;

use Config;

/**
 * An event fired when a user is confirmed and ready to register
 * @author spencerw
 *
 */
class UserConfirmed extends Event implements UserEvent {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @param \App\Confirmation $conf The confirmations table record of the confirmed user
	 * @param \App\User $user The user who confirmed the new user
	 * @return void
	 */
	public function __construct(Confirmation $conf, User $user){
		$this->conf = $conf;
		$this->user = $user;
	}

	/**
	 * The data for the view
	 * 
	 * @return array
	 */
	public function getViewData(){
		$token = $this->conf->token;
		$email = $this->conf->email;
		
		return ['action' => 'confirmed',
				'showLink' => "confirm/register?token=$token&email=$email",
				'showAdminLink' => "confirmations",
				'account' => $this->conf
		];
	}

	/**
	 * Get a collection of the users to be emailed and the views for their display
	 * @return \Illuminate\Support\Collection
	 */
	public function getMailLists(){
		$mailinglist = [];
		/*
		 * Get list of admins
		 */
		$admins = User::getAdmins();

		$mailinglist[] = collect(['list' => $admins, 'view' => 'confirmed']);		
		
		$mailinglist[] = collect(['list' => collect([$this->conf]), 'view' => 'ready']);
		
		return collect($mailinglist);
	}
}
