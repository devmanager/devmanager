<?php namespace App\Events;

use App\Events\Event;
use App\Confirmation;
use App\Contracts\Events\UserEvent;
use App\User;

use Illuminate\Queue\SerializesModels;

use Config;
/**
 * Notify administrators that a user signed up to the confirmations table
 * @author Spencer
 *
 */
class UserSignedUp extends Event implements UserEvent{

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @param \App\Confirmation $conf The confirmations table record of the signed up user
	 * @return void
	 */
	public function __construct(Confirmation $conf)
	{
		$this->conf = $conf;
	}

	/**
	 * A collection of data to show in the view
	 * @return array
	 */
	public function getViewData(){
		return ['action' => 'signed up',
				'showLink' => "confirmations",
				'account' => $this->conf
		];
	}
	
	public function getMailLists(){
		$mailinglist = [];
		/*
		 * Get list of admins
		*/
		$admins = User::getAdmins();
		
		$mailinglist[] = collect(['list' => $admins, 'view' => 'confirmed']);
		return collect($mailinglist);
	}
}
