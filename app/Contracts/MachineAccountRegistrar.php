<?php namespace App\Contracts;

/**
 * Maintains definitions for classes used to manage registering users for server accounts
 * on the machine hosting the project.
 * @author spencerw
 *
 */
interface MachineAccountRegistrar extends \Illuminate\Contracts\Auth\Registrar {
	
}
