<?php namespace App\Contracts\Events;

interface UserEvent {
	public function getViewData();
	public function getMailLists();
}