<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for records in <code>projects</code> table.
 * 
 * @author spencerw
 *
 */
class Project extends Model {
	protected $fillable = ['name', 'machine_id'];
	/**
	 * $this->machine will retrieve the <code>machines</code> record who <code>project_id</code>
	 * matches this project record's <code>id</code>.
	 * 
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function machine(){
		return $this->belongsTo('App\Machine');
	}

	/**
	 * Get the configuration data defined for this project in config/remote.php
	 * 
	 * @return array|NULL
	 */
	public function getConfigData(){
		$connectionName = $this->machine->conn;				
		$pconfigs = \Config::get("remote.connections.$connectionName.projects");
		foreach($pconfigs as $p){
			if($p['name'] === $this->name){
				return $p;
			} else {
				return null;
			}
		}
	}
}
