<?php namespace App\Dashboard;

use Illuminate\Support\Collection;
/**
 * A container for web application commands
 * @author Spencer Williams
 *
 */
class Panel {
	/**
	 * Create all commands for this panel
	 * 
	 * @param string $title The name of this panel
	 * @param string $role Sets the minimum level of access to the role as defined from the Entrust roles
	 */
	public function __construct($title, $role){
		$this->title = $title;
		$this->role = $role;
		$this->panelCommands = collect([]); 
	}
	
	/**
	 * A list of commands that are typically displayed as HTML links
	 * 
	 * @var Collection
	 */	
	protected $panelCommands = null;
	
	/**
	 * The name of the panel to be displayed
	 * @var string
	 */
	public $title = '';
	/**
	 * The role the authenticated user must have to view the contents of this panel
	 * @var string
	 */
	public $role = '';
	
	/**
	 * Add PanelCommands to this Panel
	 * 
 	 * @param string $name
	 * @param string $link
	 * @param array $permissions Keys are role names, values are array of permissions
	 * 							 or '*' for any permission
	 */
	public function createPanelCommand($name = '', $link = '', $permissions = []){
		$this->panelCommands[] = new PanelCommand($name, $link, $permissions);
	}
	
	/**
	 * Return the HTML representation of this Panel
	 * 
	 * @return string
	 */
	public function getView($viewData = []){
		$title = $this->title;
		
		$commandLinks = '';
		$this->panelCommands->each(function($command) use (&$commandLinks){
			$link = $command->link;
			$name = $command->name;			
			$commandLink = "<li><a href='$link'>$name</a></li>";
			$commandLinks .= $commandLink;
		});
		
		$commands = "<ul class='panelCommands'>$commandLinks</ul>";
		
		$view =<<<EOS
			<div class="panel col-md-4">
				<h3>$title</h3>
				$commands
			</div>
EOS;
		return $view;
	}
	
}
