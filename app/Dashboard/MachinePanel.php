<?php namespace App\Dashboard;

use App\Machine;

use Auth;

/**
 * A container for web application commands
 * @author spencerw
 *
 */
class MachinePanel extends Panel{
	/**
	 * Create all commands for this panel
	 * 
	 * @param string $modelName The fully qualified name of the model that the panel is based on
	 */
	public function __construct(){
		parent::__construct('Machine', 'admin');
		$this->createPanelCommand('Available', '/machines', ['admin' => ['*']]);
	}
	
	/**
	 * Return the HTML representation of this Panel
	 * 
	 * @return string
	 */
	public function getView($viewData = []){
		$title = $this->title;
		
		$commandLinks = '';
		
		foreach($this->panelCommands as $command){
			$link = $command->link;
			$name = $command->name;
				
			/*
			 * Only show a link to regular users if there is a machine available
			 * on which they can setup a project.
			 *
			 * If no machines are available, regular users will see a message reporting this.
			 * Otherwise, show them the link to setup the project on the machine.
			 */
			$commandText = '';
			if($command->name === 'Available'){
				if(Auth::user()->isAdmin()){
					$commandText = "<a href='$link'>$name</a>";
				}
			} else {
				$roles = collect($command->permissions);
				/*
				 * Loop through the command's allowed roles
				 */
				foreach($roles as $role => $permList){
					if(Auth::user()->hasRole($role)){
						/*
						 * If the user has the allowed role, check their permission
						 */				
						$permissions = collect($permList);
						if(empty($permList)) $permList = ['user' => ['*']];
						foreach($permissions as $permission){
							if(Auth::user()->can($permission) || $permission === '*'){
								$commandText = "<a href='$link'>$name</a>";
								break;
							}
						}
					}
					if(!empty($commandText)){
						break;
					}
				}
			}
				
			if(empty($commandText)) continue;
				
			$commandLink = "<li>$commandText</li>";
			$commandLinks .= $commandLink;
		}
		
		$commands = "<ul class='panelCommands'>$commandLinks</ul>";
		
		$view =<<<EOS
			<div class="panel col-md-4">
				<h3>$title</h3>
				$commands
			</div>
EOS;
		return $view;
	}
	
}
