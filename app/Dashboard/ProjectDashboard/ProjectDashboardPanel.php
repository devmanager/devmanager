<?php namespace App\Dashboard\ProjectDashboard;

use App\Dashboard\Panel;
use App\MachineAccount;
use App\Machine;

use Auth;
use Illuminate\Http\RedirectResponse;

class ProjectDashboardPanel extends Panel{
	/**
	 * Setup the basic components of the panel
	 * @param string $title The name of this panel
	 * @param string $role Sets the minimum level of access to the role as defined from the Entrust roles
	 */
	public function __construct($title = '', $role = ''){
		parent::__construct($title, $role);
	}
	/**
	 * Return the HTML representation of this Panel
	 *
	 * @return string
	 */
	public function getView($viewData = array()){
		$title = $this->title;
		$accounts = Auth::user()->machineAccounts();

		$view = '';
		/*
		 * Loop through each of the user's machine accounts and display the project data
		 */
		foreach($accounts as $acct){
			$project = $acct->project;
			$projectData = $project->getConfigData();
			$acctUser = escapeshellarg($acct->username);
			$machine = $acct->machine;
			
			$commandString = $projectData['getStatus'] . ' ' . $acctUser;
			$statusResponse = $machine->command($commandString);
			if(is_array($statusResponse)){
				if(!empty($statusResponse['data'])){
					$data = $statusResponse['data'];
				}
			} else if($statusResponse instanceof RedirectResponse) {
				return $statusResponse;
			} else {
				$message = 'No response data when retrieving status';
				return redirect('/machines')->with('message', $message);
			}
			
			/*
			 * Append hostname data to what is returned
			 */
			$data = ['Hostname' => $machine->host] + $data;
			$dataView = '';
			foreach($data as $label => $value){
				if($label === 'Group'){
					if(!empty($viewData['groupList'][$value])){
						$value = $viewData['groupList'][$value];
					}				
				}
				
				if($label === 'Project URL'){
					$value = "<a href='$value' target='_blank'>$value</a>";
				}
				
				$dataView .=<<<EOS
				<div class="form-group">
					<label class="control-label col-sm-4">$label</label>
					<div class="col-sm-2">
						<p class="form-control-static">$value</p>
					</div>
				</div>
EOS;
			}
			
		$view .=<<<EOS
			<div class="panel col-md-4">
				<h3>$title</h3>
				<form class="form-horizontal">
				$dataView
				</form>
			</div>
EOS;
		}
		
		return $view;
	}
}
