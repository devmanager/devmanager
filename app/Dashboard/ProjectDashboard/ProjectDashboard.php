<?php namespace App\Dashboard\ProjectDashboard;

use App\Dashboard\Dashboard;
use App\Dashboard\ProjectDashboard\ProjectDashboardPanel as Panel;

use Auth;

class ProjectDashboard extends Dashboard{
	public function __construct(){
		$this->panels = collect([]);
		
		foreach(Auth::user()->projects->all() as $project){
			$panel = new Panel($project->name, 'user');
			$this->panels[] = $panel;
		}
	}
}
