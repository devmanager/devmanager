<?php namespace App\Dashboard;

use Auth;

/**
 * A container for the panels that contain commands a user can issue to the web application
 * @author spencerw
 *
 */
class Dashboard {
	/**
	 * The number of panels to display per row
	 * @var int
	 */
	const COLUMNS_PER_ROW = 4;
	
	/**
	 * Create all the panels for all available actions
	 */
	public function __construct(){
		$this->panels = collect([]);
		
		$confirmationsPanel = new Panel('Confirmations', 'admin');
		$confirmationsPanel->createPanelCommand('Manage Accounts', '/confirmations');
		$this->panels[] = $confirmationsPanel; 
		
		// The following panels define their characteristics in their class definition
		$machinesPanel = new MachinePanel();
		$this->panels[] = $machinesPanel;
		
		$projectPanel = new ProjectPanel();
		$this->panels[] = $projectPanel;
	}
	
	/** 
	 *  The list of panels defined in the constructor in order to initialize it with an expression
	 * public $panels = collect([]);
	 * 
	 * @var Collection
	 */
	protected $panels = null;
	
	/**
	 * Show the HTML representation of the Dashboard
	 * 
	 * @return string
	 */
	public function getView($viewData = []){
		/*
		 * Get the total number of panels
		 */
		$panelTotal = count($this->panels);
		
		/*
		 * There will be 4 columns per row, so find out fewest number of rows needed
		 */
		$numOfRows = ceil($panelTotal / self::COLUMNS_PER_ROW);		
		$rows = '';
		
		/*
		 * For each row, show the panels for that row and stop when there are no more panels
		 */
		$panelsEmpty = false;
		$panelRow = '';
		for($i = 0; $i < $numOfRows; $i++){
			for($j = 0; $j < self::COLUMNS_PER_ROW; $j++){
				if($this->panels->isEmpty()){
					$panelsEmpty = true;
					break;
				}
				$panel = $this->panels->shift();
				
				/*
				 * Only show panels for which the user has the matching role
				 */
				if(!Auth::user()->hasRole($panel->role)) continue;
				
				$panelView = $panel->getView($viewData);
				
				$panelRow .= $panelView;
			}
			
			$row =<<<EOS
			<div class="row">
				$panelRow
			</div>
EOS;
			$rows .= $row;
			
			/*
			 * There are no more panels, so this is the last row.
			 */
			if($panelsEmpty) break;
		}
		
		$view =<<<EOS
			<div id="dashboard" class="container-fluid">
				$rows
			</div>				
EOS;
		return $view;
	}
}
