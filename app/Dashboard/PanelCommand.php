<?php namespace App\Dashboard;

/**
 * A container for the panels that contain commands a user can issue to the web application
 * @author spencerw
 *
 */
class PanelCommand {
	/**
	 * Create all the panels for all available actions
	 * 
	 * @param string $name The display name of this command
	 * @param string $name The link to the route that issues the command
	 * @param array $permissions An optional list of permissions the user must have to issue the command
	 */
	public function __construct($name, $link, $permissions){
		$this->name = $name;
		$this->link = $link;
		$this->permissions = $permissions;
	}
	
	/**
	 * The display name of this command
	 * @var string
	 */
	public $name = '';
	/**
	 * The link to the route that issues the command
	 * @var string
	 */
	public $link = '';
	
	/**
	 * An optional list of permissions the user must have to issue the command
	 * @var array
	 */
	public $permissions = [];
}
