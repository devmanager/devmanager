<?php namespace App\Dashboard;

use App\Machine;
use App\Project;
use App\MachineAccount;

use Auth;

/**
 * A container for commands for projects
 * @author spencerw
 *
 */
class ProjectPanel extends Panel{
	/**
	 * A list of projects available to the user for registration
	 * @var array
	 */
	public $newProjects = [];
	
	/**
	 * A list of projects available to the user to manage
	 * @var array
	 */
	public $existingProjects = [];
	/**
	 * Create all commands for this panel
	 * 
	 * @param string $modelName The fully qualified name of the model that the panel is based on
	 */
	public function __construct(){
		parent::__construct('Project', 'user');
		$this->createPanelCommand('Available', '/projects', ['admin' => ['*']]);
		
		/*
		 * Show projects that belong to the user as ready to manage, and new ones as ready
		 * for registration.
		 */
		foreach(Project::all() as $project){
			$exists = false;
			if(!Auth::user()->projects->isEmpty()){				
				foreach(Auth::user()->projects as $uproj){
					if($uproj->id === $project->id){
						$this->existingProjects[] = $project->id;
						$exists = true;						
					}
				}
			}
			if(!$exists){
				$this->newProjects[] = $project->id;
			}
			
		}
		
		/*
		 * If new projects are available, show a link to sign up for each one. 
		 */
		if(!empty($this->newProjects)){
			foreach($this->newProjects as $id){
				$project = Project::find($id);
				$name = $project->name;
				$this->createPanelCommand("Sign up for $name", "/accounts/create/$id", ['user' => ['*']]);
			}
		}
		
		if(!empty($this->existingProjects)){
			foreach($this->existingProjects as $id){
				$project = Project::find($id);
				$name = $project->name;
				$machineAcctID = MachineAccount::where('user_id', '=', Auth::user()->id)->get()->first()->id;
				$this->createPanelCommand("Manage $name", "/accounts/$machineAcctID", ['user' => ['*']]);
			}
		}
	}
	
	/**
	 * Return the HTML representation of this Panel
	 * 
	 * @return string
	 */
	public function getView($viewData = []){
		$title = $this->title;
		
		$commandLinks = '';
		
		foreach($this->panelCommands as $command){
			$link = $command->link;
			$name = $command->name;
			$commandText = '';
			
			$roles = collect($command->permissions);
			/*
			 * Loop through the command's allowed roles
			 */
			foreach($roles as $role => $permList){
				if(Auth::user()->hasRole($role)){
					/*
					 * If the user has the allowed role, check their permission
					 */				
					$permissions = collect($permList);
					if(empty($permList)) $permList = ['user' => ['*']];
					foreach($permissions as $permission){
						if(Auth::user()->can($permission) || $permission === '*'){
							$commandText = "<a href='$link'>$name</a>";
							break;
						}
					}
				}
				if(!empty($commandText)){
					break;
				}
			}
				
			if(empty($commandText)) continue;
				
			$commandLink = "<li>$commandText</li>";
			$commandLinks .= $commandLink;
		}
		
		$commands = "<ul class='panelCommands'>$commandLinks</ul>";
		
		$view =<<<EOS
			<div class="panel col-md-4">
				<h3>$title</h3>
				$commands
			</div>
EOS;
		return $view;
	}
	
}
