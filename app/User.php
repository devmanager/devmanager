<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, EntrustUserTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * Check if user is an admin
	 * @return boolean
	 */
	public function isAdmin(){
		return $this->hasRole('admin');
	}
	
	/**
	 * Get a collection of admins
	 * @return \Illuminate\Support\Collection
	 */
	public static function getAdmins(){
		$admins = self::all()->filter(function($user){
		    return $user->isAdmin();
		});
		
		return $admins;
	}
	
	/**
	 * Retrieve the user's projects
	 */
	public function projects(){
		return $this->belongsToMany('App\Project');
	}
	
	/**
	 * 
	 * @return \Illuminate\Support\Collection
	 */
	public function machineAccounts(){
		$accounts = collect([]);
		$projects = $this->projects;
		foreach($projects as $project){
			$accounts[] = MachineAccount::where('user_id' , '=', $this->id)->where('machine_id', '=', $project->machine->id)->get()->first();
		}
		return $accounts;
	}
	
	/**
	 * Add this project to the user's list maintained in the project_user pivot table
	 */
	public function addProject(Project $project){
		DB::table('project_user')->insert([
			'project_id' => $project->id,
			'user_id' => $this->id
		]);
	}
}
