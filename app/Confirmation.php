<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model {

	protected $fillable = ['email', 'token', 'confirmed'];
	protected $hidden = ['confirmed', 'remember_token'];
	
	protected $primaryKey = 'token';
}
