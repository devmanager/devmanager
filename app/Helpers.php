<?php namespace App;

/**
 * A motley assortment of sundry function of use
 * @author spencerw
 *
 */
class Helpers {
	
	/**
	 * Find out whether or not the named key (index) exists for the table. There must be a
	 * connection called <code>mysql-admin</code> defined in <code>config/database.php</code> and the user must be
	 * able to select on both tables: <code>information_schema</code> and <code>$table</code>
	 * 
	 * @param string $table
	 * @param string $key
	 * @return boolean
	 */
	public static function dbKeyExists($table = '', $key = ''){
		return !empty(\DB::connection('mysql-admin')->table('STATISTICS')->select('*')
				->where('INDEX_SCHEMA', '=', env('DB_DATABASE'))
				->where('TABLE_NAME', '=', $table)
				->where('INDEX_NAME', '=', $key)->get());
	}
	
	/**
	 * Remove the key from the table if it exists
	 * 
	 * @param string $table
	 * @param string $key
	 */
	public static function dropDBKeyIfExists($table = '', $key = ''){
		$key = 'machine_accounts_machine_id_foreign';
		if(self::dbKeyExists($table->getTable(), $key)){
			$table->dropForeign($key);
		}
	}
}