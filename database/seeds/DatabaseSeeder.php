<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;
use App\Permission;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('UserTableSeeder');
	}

}

class UserTableSeeder extends Seeder {
	public function run()
	{
		DB::table('users')->delete();
		$user = User::create(['name' => 'Spencer Williams',
					  'email' => 'williamcwilliams@gmail.com',
					  'password' => Hash::make('mypass')]);
		
		$reguser = User::create(['name' => 'Joshua',
					  'email' => 'enderandpeter@yahoo.com',
					  'password' => Hash::make('password')]);
		
		$admin = new Role();
		$admin->name = 'admin';
		$admin->display_name = 'Administrator';
		$admin->description = 'Can invite, confirm, and manage users';
		$admin->save();
		
		$user->attachRole($admin);
		
		$userrole = new Role();
		$userrole->name = 'user';
		$userrole->display_name = 'User';
		$userrole->description = 'Has the most basic access to the site';
		$userrole->save();
		
		$user->attachRole($userrole);
		$reguser->attachRole($userrole);
		
		$invite = new Permission();
		$invite->name = 'invite-users';
		$invite->display_name = 'Invite Users';
		$invite->description = 'invite new users';
		$invite->save();
		
		$confirm = new Permission();
		$confirm->name = 'confirm-users';
		$confirm->display_name = 'Confirm Users';
		$confirm->description = 'confirm new users';
		$confirm->save();
		
		$admin->attachPermission($invite);
		$admin->attachPermission($confirm);
		
		$this->command->info('Users table is ready!');
	}
}