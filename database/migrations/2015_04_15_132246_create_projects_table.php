<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('machine_id')->unsigned()->nullable();
			$table->timestamps();
			
			/*
			 * If the project's machine is removed, set the machine_id to null so that it
			 * can be setup on another machine.
			 */
			$table->foreign('machine_id')->references('id')->on('machines')->onDelete('set null');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{	
		if (Schema::hasTable('project_user'))
		{
			Schema::table('project_user', function($table)
			{
				$key = 'users_project_id_foreign';
				App\Helpers::dropDBKeyIfExists($table, $key);
			});
		}
		
		if (Schema::hasTable('users'))
		{
			Schema::table('users', function($table)
			{
				$key = 'users_project_id_foreign';
				App\Helpers::dropDBKeyIfExists($table, $key);
			});
		}
		
		if (Schema::hasTable('machine_accounts'))
		{
			Schema::table('machine_accounts', function($table)
			{
				$key = 'machine_accounts_project_id_foreign';
				App\Helpers::dropDBKeyIfExists($table, $key);
			});
		}
		
		Schema::drop('projects');
	}

}
