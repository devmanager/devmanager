<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachineAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('machine_accounts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username');
			$table->string('group');
			$table->string('shell', 50);
			/*
			 * A user must have either a password, key, or both
			 */
			$table->integer('project_id')->unsigned();
			$table->integer('machine_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
			
			$table->foreign('project_id')->references('id')->on('projects');
			$table->foreign('machine_id')->references('id')->on('machines');
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('machine_accounts');
	}

}
