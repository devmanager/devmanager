<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Create the table that keeps track of the servers that either have projects 
 * or can be setup.
 * @author Spencer
 *
 */
class CreateMachinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('machines', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('host');
			$table->string('username');
			$table->string('conn'); // From config/remote.php
			$table->string('group', 32); // The web app will run commmands on behalf of users in this group
			// The length of an SSH RSA key
			$table->string('key', 381);
			// Whether or not this machine is available to setup projects
			$table->boolean('enabled')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('projects'))
		{
			Schema::table('projects', function($table)
			{
				$key = 'projects_machine_id_foreign';
				App\Helpers::dropDBKeyIfExists($table, $key);
			});
		}
		
		if (Schema::hasTable('machine_accounts'))
		{
			Schema::table('machine_accounts', function($table)
			{
				$key = 'machine_accounts_machine_id_foreign';
				App\Helpers::dropDBKeyIfExists($table, $key);
			});
		}
		
		Schema::drop('machines');
	}

}
