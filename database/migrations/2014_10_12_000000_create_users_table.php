<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		if (Schema::hasTable('project_user'))
		{
			Schema::table('project_user', function($table)
			{
				$key = 'project_user_user_id_foreign';
				App\Helpers::dropDBKeyIfExists($table, $key);			
			});
		}
		
		if (Schema::hasTable('machine_accounts'))
		{
			Schema::table('machine_accounts', function($table)
			{
				$key = 'machine_accounts_user_id_foreign';
				App\Helpers::dropDBKeyIfExists($table, $key);
			});
		}
		Schema::drop('users');
	}

}
