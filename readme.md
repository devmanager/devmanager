# Dev Manager
A [Laravel](http://laravel.com/) web application to assist developers in securely setting up web-based projects on a remote server.

## How can I use this utility?
This utility consists of the Laravel application that runs on the server that will be used to manage access to the web app itself, as well as 
scripts/programs that are executed by a user on the development server that the web app will use to issue all of its commands on the remote machine. The remote computer
is the server hosting all of the developers' projects. Dev Manager is geared towards setting up users for web-based projects that are hosted on the remote
server. There are [various separate components](https://gitlab.com/groups/devmanager) that all work in tandem to allow the web application to securely
and reliably communicate with the project server.

## Installation
When you first clone the project on the server that will host the web application, you will need to create the database. You can easily create everything from the migration and seed files. Just be sure to set the database name, username, and password in .env.

Please see the .env.example file, which mentions the `DB_ADMIN` values needed to connect to the database as a user that can select from both the `information_schema.STATISTICS` table and all tables in the web app’s database. This admin user is for checking the existence of foreign keys before removing them when migrations are rolled back.

If your database and connection is configured properly, you should be able to run the following artisan command:

    php artisan migrate:refresh --seed
    
To serve the web application, just make the `/public` folder, which contains the index.php file, the site document root that starts off the web app. Also, make sure the web server allows the 
.htaccess file and its directives. Laravel's [installation guide](http://laravel.com/docs/5.0/installation) should also be of use.

With the example users created and your web server setup to serve the Laravel application, you can visit the project home page and sign in as an admin with these credentials:

    username: williamcwilliams@gmail.com
    password: mypass

You will want to change the email in the `users` table to an address you have access to so that you can recieve email notifications. 

The database seeder files in `/database/seeds` will setup a few example users. The [Entrust](https://github.com/Zizaco/entrust) package is used to help manage 
permissions and roles.

## Registration
New users can sign up by clicking the Register link at the `/login` page. Enter an email address and click Sign Up to send the confirmation email to admins. Admins will receive email notification, and can either approve or decline the request. If approved, the user gets an email to a link inviting them to create an account. New users will be able to sign up for and manage any available projects.

## Remote Server Setup
Dev Manager needs to be able to make SSH connections to the machine that will host the projects it will manage. The web application uses the [Laravel Collective SSH](http://laravelcollective.com/docs/5.0/ssh) package to manage such communication. You will need a remote.php file in the config folder, and the one from the [example Dev Camp project](https://gitlab.com/devmanager/dc-projects) is the best to start with.

The files named in the configuration array for the Dev Camp project in remote.php refer to scripts on the remote server. These scripts are also available in the [Dev Camp - Server Scripts](https://gitlab.com/devmanager/dc-serverscripts) repo.

The remote server must have a user named setupadmin that can issue commands on behalf of users in the project’s managing group, which is `developers` for the Dev Camp project. Be sure to add the [example sudoers file](https://gitlab.com/devmanager/sudoers-devmanager) in the `/etc/sudoers.d` folder on the remote machine, which defines the sudo privileges the web app user needs in order to properly run the scripts.

## Project Initialization
Although the data defining the project is in remote.php, the web application needs to confirm that it can connect to the machine and issue the kinds of commands 
the project needs. Admins will need to go to Machine -> Available from the Dashboard, find the listing for the remote connection defined in remote.php, and click
Enable. You will be taken to a form that shows the machine's setup from the config file. Submit the form to test the connection. If successful, the Dashboard
will show the Sign Up link for the project.

## Project Sign Up
Once a user is registered, they will see invitations for available projects that admins have setup. On clicking a Sign Up link, they can enter their desired
credentials for creating an account on the remote server. If all goes well, they will be registered and ready to manage the project.

## Dev Camp - Example Project
Dev Manager can be used to automate any tasks on the machine that are defined in the Server Scripts. Use them as a guide to understand how the web application is expecting to communicate. After the web app executes a script on the remote server, it simply expects to receive JSON  with a `status` key for the command status string (e.g., Success, Error, Warning, etc.), a `message` key for a string message of any length, and an optional `data` key with any data returned by the scripts. The server scripts can execute any actions desired and simply need to return a JSON object with the two required key-pairs and the optional third one.

The Dev Camp project simply allows a developer to create an account on the machine which they will use to manage their SFTP-only connection to the server. Each user signs up for a team, which corresponds to a group on the machine. They will only be able to write to their group folder in the web projects directory at `/var/www`. The server needs to be configured, such as with httpd, to host sites based in the group folders with distinct URLs, preferably named after the groups. Only members of the corresponding group should be allowed to write to group-owned folder.

<hr>

Any and all feedback is welcome. Thank you for trying out this project!